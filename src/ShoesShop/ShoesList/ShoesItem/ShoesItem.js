import React, { Component } from "react";

export default class ShoesItem extends Component {
  render() {
    return (
      <div className='col-lg-3 col-md-4 col-sm-6 p-2'>
        <div className='card h-100 text-left'>
          <img
            className='card-img-top'
            src={this.props.data.image}
            alt='Card cap'
          />
          <div className='card-body'>
            <h5 className='card-title'>{this.props.data.name}</h5>
            <p className='card-text'>{this.props.data.shortDescription}</p>
            <a
              href='##'
              onClick={() => {
                this.props.btnAddToCart(this.props.data);
              }}
              className='btn btn-primary'
            >
              Buy
            </a>
            <a
              href='##'
              onClick={() => {
                this.props.btnShowDetails(this.props.data);
              }}
              className='btn btn-primary'
            >
              Details
            </a>
          </div>
        </div>
      </div>
    );
  }
}
