import React, { Component } from "react";
import ShoesItem from "./ShoesItem/ShoesItem";

export default class ShoesList extends Component {
  renderShoesList = () => {
    return this.props.data.map((shoes) => {
      return (
        <ShoesItem
          btnAddToCart={this.props.btnAddToCart}
          btnShowDetails={this.props.btnShowDetails}
          data={shoes}
        />
      );
    });
  };

  render() {
    return (
      <div className='container'>
        <div className='row'>{this.renderShoesList()}</div>
      </div>
    );
  }
}
