import React, { Component } from "react";
import Cart from "./Cart/Cart";
import ShoesList from "./ShoesList/ShoesList";
import ShoesDetails from "./ShoesDetails/ShoesDetails";
import { shoesData } from "../shoesData/shoesData";

export default class ShoesShop extends Component {
  state = {
    cart: [],
    shoes: shoesData,
    details: shoesData[0],
  };

  btnAddToCart = (shoesObj) => {
    const cloneCart = [...this.state.cart];

    const index = cloneCart.findIndex((item) => {
      return item.id === shoesObj.id;
    });

    if (index === -1) {
      cloneCart.push({ ...shoesObj, quantity: 1 });
    } else {
      cloneCart[index].quantity++;
    }

    this.setState({
      cart: cloneCart,
    });
  };

  btnShowDetails = (shoesObj) => {
    this.setState({
      details: shoesObj,
    });
  };

  btnAddQuantity = (shoesObj) => {
    this.btnAddToCart(shoesObj);
  };

  btnReduceQuantity = (shoesObj) => {
    const cloneCart = [...this.state.cart];

    const index = cloneCart.findIndex((item) => {
      return item.id === shoesObj.id;
    });

    cloneCart[index].quantity--;

    if (cloneCart[index].quantity === 0) {
      const newArr = cloneCart.filter((item) => {
        return item.id !== shoesObj.id;
      });

      this.setState({
        cart: newArr,
      });
    } else {
      this.setState({
        cart: cloneCart,
      });
    }
  };

  render() {
    return (
      <div>
        <Cart
          quantity={this.state.quantity}
          btnReduceQuantity={this.btnReduceQuantity}
          btnAddQuantity={this.btnAddQuantity}
          data={this.state.cart}
        />
        <ShoesList
          btnShowDetails={this.btnShowDetails}
          btnAddToCart={this.btnAddToCart}
          data={this.state.shoes}
        />
        <ShoesDetails data={this.state.details} />
      </div>
    );
  }
}
